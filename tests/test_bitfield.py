from series7.bitfield import IntBitfield


class BitFieldSingleBit(IntBitfield):
    _bitfields = {
        'bit0_a': 0,
        'bit0_b': (0, ),
        'bit0_c': {
            'msb': 0
        },
        'bit0_d': {
            'msb': 0,
            'lsb': 0
        },
        'bit0_e': (0, 0)
    }


def test_repr():
    bitfield = BitFieldSingleBit()
    assert repr(bitfield) == "BitFieldSingleBit(bit0_a=False, bit0_b=False, " \
                             "bit0_c=False, bit0_d=0, bit0_e=0, bits=0)"
    bitfield.bits = 11
    assert repr(bitfield) == "BitFieldSingleBit(bit0_a=True, bit0_b=True, " \
                             "bit0_c=True, bit0_d=1, bit0_e=1, bits=11)"


def test_single_bit_fields():
    bitfield = BitFieldSingleBit()
    assert bitfield.bit0_a is False
    assert bitfield.bit0_b is False
    assert bitfield.bit0_c is False
    assert bitfield.bit0_d == 0
    assert bitfield.bit0_e == 0
    assert bitfield.bits == 0

    bitfield.bit0_a = True
    assert bitfield.bit0_a is True
    assert bitfield.bit0_b is True
    assert bitfield.bit0_c is True
    assert bitfield.bit0_d == 1
    assert bitfield.bit0_e == 1
    assert bitfield.bits == 1

    bitfield.bit0_a = 0
    assert bitfield.bit0_a is False
    assert bitfield.bit0_b is False
    assert bitfield.bit0_c is False
    assert bitfield.bit0_d == 0
    assert bitfield.bit0_e == 0
    assert bitfield.bits == 0

    bitfield.bit0_d = 0xFF
    assert bitfield.bit0_a is True
    assert bitfield.bit0_b is True
    assert bitfield.bit0_c is True
    assert bitfield.bit0_d == 1
    assert bitfield.bit0_e == 1
    assert bitfield.bits == 1

    bitfield.bit0_a = 0xFF
    assert bitfield.bit0_a is True
    assert bitfield.bit0_b is True
    assert bitfield.bit0_c is True
    assert bitfield.bit0_d == 1
    assert bitfield.bit0_e == 1
    assert bitfield.bits == 1


class BitFieldMultipleBits(IntBitfield):
    _bitfields = {
        'field1': (10, 0),
        'field2': {
            'msb': 10,
            'lsb': 0
        },
        'field3': (11, 1),
        'field4': {
            'msb': 11,
            'lsb': 1,
            'from_bits': float
        },
    }


def test_multiple_bits_fields():
    bitfield = BitFieldMultipleBits()

    assert bitfield.field1 == 0
    assert bitfield.field2 == 0
    assert bitfield.field3 == 0
    assert bitfield.field4 == 0.0
    assert bitfield.bits == 0
    assert type(bitfield.field4) is float

    bitfield.field1 = 0xFFF
    assert bitfield.field1 == 0b11111111111
    assert bitfield.field2 == 0b11111111111
    assert bitfield.field3 == 0b01111111111
    assert bitfield.field4 == float(bitfield.field3)
    assert bitfield.bits == 0b11111111111

    bitfield.field1 = 0x0
    assert bitfield.field1 == 0
    assert bitfield.field2 == 0
    assert bitfield.field3 == 0
    assert bitfield.field4 == 0.0
    assert bitfield.bits == 0

    bitfield.field3 = 0xFFF
    assert bitfield.field1 == 0b11111111110
    assert bitfield.field2 == 0b11111111110
    assert bitfield.field3 == 0b11111111111
    assert bitfield.field4 == float(bitfield.field3)
    assert bitfield.bits == 0b111111111110


def test_multiple_bits_fields_init():
    bitfield = BitFieldMultipleBits(field1=0xFFF, field3=0)
    assert bitfield.field1 == 1
    assert bitfield.field2 == 1
    assert bitfield.field3 == 0
    assert bitfield.field4 == float(bitfield.field3)
    assert bitfield.bits == 1
