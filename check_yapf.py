#!/bin/env python3

import subprocess
import sys

if __name__ == '__main__':
    out = subprocess.check_output(['yapf', '--recursive', '--diff'] +
                                  sys.argv[1:])
    sys.stdout.buffer.write(out)
    sys.exit(len(out) > 0)
