import setuptools

requirements = [
    'attrs>=17.2', 'Click>=6.0', 'Jinja2>=2.9', 'numpy>=1.13',
    'simplejson>=3.11'
]

setuptools.setup(
    name="series7",
    version="0.1.0",
    url="https://github.com/dzamlo/series7",
    author="Loïc Damien",
    author_email="loic.damien@dzamlo.ch",
    description="Reverse engineering of the 7 series FPGA bitstream.",
    long_description=open('README.md').read(),
    packages=['series7'],
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'add_frames_addresses_to_json=\
            series7.scripts.add_frames_addresses_to_json:main',
            'frames_diff=series7.scripts.frames_diff:main',
            'parse_bitstream=series7.scripts.parse_bitstream:main',
            'program_fpga=series7.scripts.program_fpga:main'
        ],
    },
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ], )
