from tempfile import NamedTemporaryFile
from textwrap import dedent

from jinja2 import Template

from .vivado import run_vivado_with_vhdl

EMPTY_VHDL = """\
entity top is
end top;
"""


def dump_sites(part,
               out_filename,
               get_sites_extra_args="",
               include_bels=False,
               include_bel_pins=False,
               include_site_pips=False,
               include_site_pins=False):
    tcl_template = r"""
    synth_design -top top -part {{part}}
    set f [open "{{out_filename}}" "w"]

    puts $f "\{"
    set site_delimiter ""
    foreach site [get_sites {{get_sites_extra_args}}] {
        puts $f "$site_delimiter\"$site\" : \{"

        set site_property_delimiter ""
        foreach property [list_property $site] {
            set value [get_property $property $site]
            puts $f "$site_property_delimiter\"$property\": \"$value\""
            set site_property_delimiter ","
        }

        {% if include_bels -%}
        puts $f ",\"BELS\": \{"
        set bel_delimiter ""
        foreach bel [get_bels -of $site] {
            puts $f "$bel_delimiter\"$bel\": \{"

            set bel_property_delimiter ""
            foreach property [list_property $bel] {
                set value [get_property $property $bel]
                puts $f "$bel_property_delimiter\"$property\": \"$value\""
                set bel_property_delimiter ","
            }

            {% if include_bel_pins -%}
            puts $f "$bel_property_delimiter\"BEL_PINS\": \{"
            set bel_pin_delimiter ""
            foreach bel_pin [get_bel_pins -of $bel] {
                puts $f "$bel_pin_delimiter\"$bel_pin\": \{"
                set bel_pin_property_delimiter ""
                foreach property [list_property $bel_pin] {
                    set value [get_property $property $bel_pin]
                    puts $f "$bel_pin_property_delimiter\"$property\": \"$value\""
                    set bel_pin_property_delimiter ","
                }
                puts  $f "\}"
                set bel_pin_delimiter ","
            }
            puts $f "\}"
            {% endif %}

            puts $f "\}"
            set bel_delimiter ","
        }
        puts $f "\}"
        {% endif -%}
        {% if include_site_pips %}
        puts $f ",\"SITE_PIPS\": \{"
        set site_pip_delimiter ""
        foreach site_pip [get_site_pips -of $site] {
            puts $f "$site_pip_delimiter\"$site_pip\": \{"
            set site_pip_property_delimiter ""
            foreach property [list_property $site_pip] {
                # reading the value of IS_FIXED crash vivado
                if {$property != "IS_FIXED"} {
                    set value [get_property $property $site_pip]
                    puts $f "$site_pip_property_delimiter\"$property\": \"$value\""
                    set site_pip_property_delimiter ","
                }

            }

            puts $f "\}"
            set site_pip_delimiter ","
        }
        puts $f "\}"        
        {% endif %}

        {% if include_site_pins %}
        puts $f ",\"SITE_PINS\": \{"
        set site_pin_delimiter ""
        foreach site_pin [get_site_pins -of $site] {
            puts $f "$site_pin_delimiter\"$site_pin\": \{"
            set site_pin_property_delimiter ""
            foreach property [list_property $site_pin] {
                set value [get_property $property $site_pin]
                puts $f "$site_pin_property_delimiter\"$property\": \"$value\""
                set site_pin_property_delimiter ","
            }

            puts $f "\}"
            set site_pin_delimiter ","
        }
        puts $f "\}"        
        {% endif %}


        puts $f "\}"
        set site_delimiter ","
    }
    puts $f "\}"
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    tcl = tcl_template.render(
        part=part,
        out_filename=out_filename,
        get_sites_extra_args=get_sites_extra_args,
        include_bels=include_bels,
        include_bel_pins=include_bel_pins,
        include_site_pips=include_site_pips,
        include_site_pins=include_site_pins)
    run_vivado_with_vhdl(tcl, EMPTY_VHDL)


def dump_property(part, out_filename, get_command):
    tcl_template = r"""
    synth_design -top top -part {{part}}
    set f [open "{{out_filename}}" "w"]

    puts $f "\{"
    set element_delimiter ""
    foreach element [{{get_command}}] {
        puts $f "$element_delimiter\"$element\" : \{"

        set property_delimiter ""
        foreach property [list_property $element] {
            set value [get_property $property $element]
            puts $f "$property_delimiter\"$property\": \"$value\""
            set property_delimiter ","
        }
        puts $f "\}"
        set element_delimiter ","
    }
    puts $f "\}"
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    tcl = tcl_template.render(
        part=part, out_filename=out_filename, get_command=get_command)

    run_vivado_with_vhdl(tcl, EMPTY_VHDL)


def slices(part):
    tcl_template = """
    synth_design -top top -part {{part}}
    set f [open "{{out_filename}}" "w"]
    foreach slice [get_sites SLICE_*] {
       puts $f $slice
    }
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    with NamedTemporaryFile("rt", encoding="utf-8") as f:
        tcl = tcl_template.render(part=part, out_filename=f.name)
        run_vivado_with_vhdl(tcl, EMPTY_VHDL)
        return f.read().splitlines()


def slice_count(part):
    tcl_template = """
    synth_design -top top -part {{part}}
    set f [open "{{out_filename}}" "w"]
    puts $f [llength  [get_sites SLICE_*]]
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    with NamedTemporaryFile("rt", encoding="utf-8") as f:
        tcl = tcl_template.render(part=part, out_filename=f.name)
        run_vivado_with_vhdl(tcl, EMPTY_VHDL)
        return int(f.read())
