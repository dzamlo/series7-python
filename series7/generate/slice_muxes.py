import pickle
from random import choice
from textwrap import dedent

from jinja2 import Template

from .vivado import run_vivado_with_vhdl

# PRECYINIT is not included because CIN is not set correctly with
# `set_property SITE_PIPS`
MUXES_SLICEL = {
    'A5FFMUX': ['', 'IN_A', 'IN_B'],
    'ACY0': ['AX', 'O5'],
    'AFFMUX': ['', 'AX', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'AOUTMUX': ['', 'A5Q', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'B5FFMUX': ['', 'IN_A', 'IN_B'],
    'BCY0': ['BX', 'O5'],
    'BFFMUX': ['', 'BX', 'CY', 'F8', 'O5', 'O6', 'XOR'],
    'BOUTMUX': ['', 'B5Q', 'CY', 'F8', 'O5', 'O6', 'XOR'],
    'C5FFMUX': ['', 'IN_A', 'IN_B'],
    'CCY0': ['CX', 'O5'],
    'CEUSEDMUX': ['1', 'IN'],
    'CFFMUX': ['', 'CX', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'CLKINV': ['CLK', 'CLK_B'],
    'COUTMUX': ['', 'C5Q', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'D5FFMUX': ['', 'IN_A', 'IN_B'],
    'DCY0': ['DX', 'O5'],
    'DFFMUX': ['', 'CY', 'DX', 'O5', 'O6', 'XOR'],
    'DOUTMUX': ['', 'CY', 'D5Q', 'O5', 'O6', 'XOR'],
    'SRUSEDMUX': ['0', 'IN']
}

# *DI1MUX are not included because they are not set correctly with
# `set_property SITE_PIPS`
MUXES_SLICEM = {
    'A5FFMUX': ['', 'IN_A', 'IN_B'],
    'ACY0': ['AX', 'O5'],
    'AFFMUX': ['', 'AX', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'AOUTMUX': ['', 'A5Q', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'B5FFMUX': ['', 'IN_A', 'IN_B'],
    'BCY0': ['BX', 'O5'],
    'BFFMUX': ['', 'BX', 'CY', 'F8', 'O5', 'O6', 'XOR'],
    'BOUTMUX': ['', 'B5Q', 'CY', 'F8', 'O5', 'O6', 'XOR'],
    'C5FFMUX': ['', 'IN_A', 'IN_B'],
    'CCY0': ['CX', 'O5'],
    'CEUSEDMUX': ['1', 'IN'],
    'CFFMUX': ['', 'CX', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'CLKINV': ['CLK', 'CLK_B'],
    'COUTMUX': ['', 'C5Q', 'CY', 'F7', 'O5', 'O6', 'XOR'],
    'D5FFMUX': ['', 'IN_A', 'IN_B'],
    'DCY0': ['DX', 'O5'],
    'DFFMUX': ['', 'CY', 'DX', 'MC31', 'O5', 'O6', 'XOR'],
    'DOUTMUX': ['', 'CY', 'D5Q', 'MC31', 'O5', 'O6', 'XOR'],
    'SRUSEDMUX': ['0', 'IN'],
    'WA7USED': ['', '0'],
    'WA8USED': ['', '0'],
    'WEMUX': ['CE', 'WE']
}


def slice_muxes(muxes_valuess, out_prefix, part, slice_type):
    tcl_template = """
    synth_design -top top -part {{part}}
    place_design
    route_design
    {% for slice in muxes_valuess[0] -%}
    set_property MANUAL_ROUTING {{slice_type}} [get_sites {{slice}}]
    {% endfor %}
    
    {% for muxes_values in muxes_valuess %}
    {% for slice in muxes_values -%}
    set_property SITE_PIPS {{muxes_values[slice]}} [get_sites {{slice}}]
    {% endfor %}
    write_bitstream -force {{out_prefix}}{{loop.index0}}
    {% endfor %}
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    # we put at least one element in each slices, otherwise SITE_PIPS is
    # ignored.
    vhdl_template = """
    library UNISIM;
    use UNISIM.VComponents.all;

    entity top is
    end top;

    architecture Behavioral of top is
    attribute DONT_TOUCH : string;
    attribute LOC : string;
    {% for slice in slices %}
    attribute DONT_TOUCH of {{slice}}: label is "TRUE";
    attribute LOC of {{slice}}: label is "{{slice}}";
    {% endfor %}
    begin
        {% for slice in slices %}
        {{slice}} : LUT1
        generic map (
            INIT => "01")
        port map (
            I0 => '0'
        );
        {% endfor %}
    end Behavioral;
    """  # noqa
    vhdl_template = Template(dedent(vhdl_template))

    vhdl = vhdl_template.render(slices=muxes_valuess[0].keys())
    tcl = tcl_template.render(
        part=part,
        muxes_valuess=muxes_valuess,
        out_prefix=out_prefix,
        slice_type=slice_type)
    run_vivado_with_vhdl(tcl, vhdl)


def random_muxes_values(slices, muxes):
    return {
        slice: {name: choice(values)
                for (name, values) in muxes.items()}
        for slice in slices
    }


def muxes_values_dict_to_str(muxes_values_dict):
    return "{ " + " ".join(f"{name}:{value}"
                           for (name, value) in muxes_values_dict.items()
                           if value != '') + " }"


def random_slices_muxes(n, out_prefix, part, slices, slice_type, muxes=None):
    if muxes is None:
        if slice_type == "SLICEL":
            muxes = MUXES_SLICEL
        elif slice_type == "SLICEM":
            muxes = MUXES_SLICEM
    muxes_valuess = [random_muxes_values(slices, muxes) for _ in range(n)]
    with open(f"{out_prefix}values", 'wb') as f:
        values = []
        for slice in slices:
            for mux in muxes_valuess[0][slice]:
                name = f'{slice}/{mux}'
                values.append((name, [
                    muxes_values[slice][mux] for muxes_values in muxes_valuess
                ]))
        pickle.dump(values, f)

    for muxes_values in muxes_valuess:
        for slice in muxes_values:
            muxes_values[slice] = muxes_values_dict_to_str(muxes_values[slice])

    slice_muxes(muxes_valuess, out_prefix, part, slice_type)


def random_slices_muxes_region(n,
                               out_prefix,
                               part,
                               slice_type,
                               start_x,
                               end_x,
                               start_y,
                               end_y,
                               muxes=None):
    slices = [
        f"SLICE_X{x}Y{y}"
        for x in range(start_x, end_x + 1) for y in range(start_y, end_y + 1)
    ]
    random_slices_muxes(n, out_prefix, part, slices, slice_type, muxes=muxes)
