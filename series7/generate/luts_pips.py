from random import choice, random
from textwrap import dedent

from jinja2 import Template

from .vivado import run_vivado_with_vhdl

BELS = ['A', 'B', 'C', 'D']


def luts_pips(out_prefix, part, slices, connections):
    tcl_template = """
    synth_design -top top -part {{part}}
    place_design
    route_design
    write_bitstream -force {{out_prefix}}
    set f [open "{{out_prefix}}.pips" "w"]
    foreach pip [get_pips -of_objects [get_nets]] {
        puts $f $pip
    }
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    vhdl_template = """
    library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    library UNISIM;
    use UNISIM.VComponents.all;

    entity top is
    end top;

    architecture Behavioral of top is
        attribute DONT_TOUCH : string;
        attribute LOC : string;
        attribute BEL : string;
        {% for slice in slices -%}
        {% for bel in ['A', 'B', 'C', 'D'] %}
        attribute DONT_TOUCH of {{slice}}_{{bel}}: label is "TRUE";
        attribute LOC of {{slice}}_{{bel}}: label is "{{slice}}";
        attribute BEL of {{slice}}_{{bel}}: label is "{{bel}}6LUT";
        signal I0{{slice}}_{{bel}}: std_logic;
        signal I1{{slice}}_{{bel}}: std_logic;
        signal I2{{slice}}_{{bel}}: std_logic;
        signal I3{{slice}}_{{bel}}: std_logic;
        signal I4{{slice}}_{{bel}}: std_logic;
        signal I5{{slice}}_{{bel}}: std_logic;
        signal O{{slice}}_{{bel}}: std_logic;
        {% endfor -%}
        {% endfor -%}
    begin
        {% for slice in slices -%}
        {% for bel in ['A', 'B', 'C', 'D'] %}
        {{slice}}_{{bel}} : LUT6
        generic map (
            INIT => X"0000000000000000")
        port map (
            I0 => I0{{slice}}_{{bel}},
            I1 => I1{{slice}}_{{bel}},
            I2 => I2{{slice}}_{{bel}},
            I3 => I3{{slice}}_{{bel}},
            I4 => I4{{slice}}_{{bel}},
            I5 => I5{{slice}}_{{bel}},
            O => O{{slice}}_{{bel}}
        );
        {% endfor -%}
        {% endfor -%}
        {% for (dst, src) in connections %}
        {{dst}} <= {{src}};
        {% endfor %}
    end Behavioral;
    """  # noqa
    vhdl_template = Template(dedent(vhdl_template))

    vhdl = vhdl_template.render(slices=slices, connections=connections)
    tcl = tcl_template.render(
        part=part, out_prefix=out_prefix, slices=slices)
    run_vivado_with_vhdl(tcl, vhdl)


def random_connections(slices, connection_probability=0.5):
    srcs = [f'O{slice}_{bel}' for bel in BELS for slice in slices]
    srcs += ["'0'", "'1'"]
    connections = [] #[(f'I{i}{slice}_{bel}', choice(srcs)) for i in range(6) for bel in BELS for slice in slices if ra]
    for slice in slices:
        for bel in BELS:
            for i in range(6):
                if random() < connection_probability:
                    connections.append((f'I{i}{slice}_{bel}', choice(srcs)))

    return connections


def random_luts_pips(n, out_prefix, part, slices):
    for i in range(n):
        connections = random_connections(slices)
        luts_pips(f'{out_prefix}{i}', part, slices, connections)


def random_luts_pips_region(n, out_prefix, part, start_x, end_x,
                                    start_y, end_y):
    slices = [
        f"SLICE_X{x}Y{y}"
        for x in range(start_x, end_x + 1) for y in range(start_y, end_y + 1)
    ]
    random_luts_pips(n, out_prefix, part, slices)
