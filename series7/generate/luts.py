import pickle
from random import randrange
from textwrap import dedent

from jinja2 import Template

from .vivado import run_vivado_with_vhdl

LUT_INIT_LENGTH = 64


def random_inits(slices):
    for slice in slices:
        for bel in ['A', 'B', 'C', 'D']:
            yield ((slice, bel), randrange(2**LUT_INIT_LENGTH))


def multiples_lut6(initss, out_prefix, part, slices):
    tcl_template = """
    synth_design -top top -part {{part}}
    place_design
    route_design
    {% for inits in initss %}
    {% for slice in slices -%}
    {% for bel in ['A', 'B', 'C', 'D'] %}
    set_property INIT 64'h{{"%016X"|format(inits[(slice,bel)])}} [get_cells {{slice}}_{{bel}}]
    {% endfor %}
    {% endfor %}
    write_bitstream -force {{out_prefix}}{{loop.index0}}
    {% endfor %}
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    vhdl_template = """
    library UNISIM;
    use UNISIM.VComponents.all;

    entity top is
    end top;

    architecture Behavioral of top is
        attribute DONT_TOUCH : string;
        attribute LOC : string;
        attribute BEL : string;
        {% for slice in slices -%}
        {% for bel in ['A', 'B', 'C', 'D'] %}
        attribute DONT_TOUCH of {{slice}}_{{bel}}: label is "TRUE";
        attribute LOC of {{slice}}_{{bel}}: label is "{{slice}}";
        attribute BEL of {{slice}}_{{bel}}: label is "{{bel}}6LUT";
        {% endfor -%}
        {% endfor -%}
    begin
        {% for slice in slices -%}
        {% for bel in ['A', 'B', 'C', 'D'] %}
        {{slice}}_{{bel}} : LUT6
        generic map (
            INIT => X"0000000000000000")
        port map (
            I0 => '0',
            I1 => '0',
            I2 => '0',
            I3 => '0',
            I4 => '0',
            I5 => '0'
        );
        {% endfor -%}
        {% endfor -%}
    end Behavioral;
    """  # noqa
    vhdl_template = Template(dedent(vhdl_template))

    vhdl = vhdl_template.render(slices=slices)
    tcl = tcl_template.render(
        part=part, initss=initss, out_prefix=out_prefix, slices=slices)
    run_vivado_with_vhdl(tcl, vhdl)


def random_luts(n, out_prefix, part, slices):
    initss = [dict(random_inits(slices)) for _ in range(n)]

    with open(f"{out_prefix}values", 'wb') as f:
        values = []
        for lut in initss[0]:
            for bit in range(LUT_INIT_LENGTH):
                name = f"{lut[0]}/{lut[1]}6LUT.INIT[{bit}]"
                bits = [int((inits[lut] >> bit) & 1) for inits in initss]
                values.append((name, bits))
        pickle.dump(values, f)

    multiples_lut6(initss, out_prefix, part, slices)


def random_luts_region(n, out_prefix, part, start_x, end_x, start_y, end_y):
    slices = [
        f"SLICE_X{x}Y{y}"
        for x in range(start_x, end_x + 1) for y in range(start_y, end_y + 1)
    ]
    random_luts(n, out_prefix, part, slices)
