import pickle
from random import choices
from textwrap import dedent

from jinja2 import Template

from .vivado import vhdl_to_bitstream

CARRY_IN_VALUES = ['0', '1', 'AX', 'CIN']


def carry_in(values, x, start_y, out_prefix, part):
    vhdl_template = """
    library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    library UNISIM;
    use UNISIM.VComponents.all;

    entity top is
    end top;

    architecture Behavioral of top is
        attribute DONT_TOUCH : string;
        attribute LOC : string;
        {% for i in range(n_slices) -%}
        attribute DONT_TOUCH of SLICE_X{{x}}Y{{start_y+i}}_CARRY: label is "TRUE";
        attribute LOC of SLICE_X{{x}}Y{{start_y+i}}_CARRY: label is "SLICE_X{{x}}Y{{start_y+i}}";
        {% endfor -%}
        
        signal C: std_logic_vector({{4*n_slices}} downto 0);
    begin
    
        {% for i in range(n_slices) -%}
        SLICE_X{{x}}Y{{start_y+i}}_CARRY : CARRY4
        port map (
        {% if values[i] == '0' %}
        CI => '0',
        CYINIT => '0',
        {% elif values[i] == '1' %}
        CI => '0',
        CYINIT => '1',
        {% elif values[i] == 'AX' %}
        CI => '0',
        CYINIT => C(0),
        {% elif values[i] == 'CIN' %}
        CI => C({{i*4-1}}),
        CYINIT => '0',
        {% endif %}
        DI(0) => C(0),
        DI(3 downto 1) => "000",
        S => "0000",
        CO => C({{(i+1)*4-1}} downto {{i*4}})
        );
        {% endfor -%}

    end Behavioral;
    """  # noqa
    vhdl_template = Template(dedent(vhdl_template))

    vhdl = vhdl_template.render(
        values=values, x=x, start_y=start_y, n_slices=len(values))
    vhdl_to_bitstream(vhdl, out_prefix, part)


def random_carry_in(n, out_prefix, part, x, start_y, end_y):
    slices = [f'SLICE_X{x}Y{y}' for y in range(start_y, end_y + 1)]
    n_slices = len(slices)
    valuess = [choices(CARRY_IN_VALUES, k=n_slices) for _ in range(n)]
    for values in valuess:
        if values[0] == 'CIN':
            values[0] = 'AX'

    with open(f"{out_prefix}values", 'wb') as f:
        values = []
        for (i, slice) in enumerate(slices):
            name = f"{slice}/PRECYINIT"
            value = [v[i] for v in valuess]
            values.append((name, value))
        pickle.dump(values, f)

    for (i, values) in enumerate(valuess):
        carry_in(values, x, start_y, f'{out_prefix}{i}', part)
