from subprocess import run
from tempfile import NamedTemporaryFile, TemporaryDirectory
from textwrap import dedent

from jinja2 import Template


def run_vivado(tcl):
    with TemporaryDirectory() as tmpdir:
        args = [
            'vivado', '-nolog', '-nojournal', '-mode', 'tcl', '-tempDir',
            tmpdir
        ]
        return run(args, encoding='utf-8', input=tcl)


def run_vivado_with_vhdl(tcl, vhdl):
    with NamedTemporaryFile("w+t", encoding="utf-8") as f:
        f.write(vhdl)
        f.flush()
        tcl = f"read_vhdl {f.name}" + tcl
        run_vivado(tcl)


def vhdl_to_bitstream(vhdl,
                      bitstream_name,
                      part,
                      top_name="top",
                      synth_extra_args="",
                      write_bitstream_extra_args="",
                      start_gui=False):
    tcl_template = """
    synth_design -top {{top_name}} -part {{part}} {{synth_extra_args}}
    place_design
    route_design
    {% if start_gui %}
    start_gui
    {% else %}
    write_bitstream {{write_bitstream_extra_args}} -force {{bitstream_name}}
    {% endif %}
    """
    tcl_template = Template(dedent(tcl_template))
    tcl = tcl_template.render(
        bitstream_name=bitstream_name,
        part=part,
        top_name=top_name,
        synth_extra_args=synth_extra_args,
        write_bitstream_extra_args=write_bitstream_extra_args,
        start_gui=start_gui)
    run_vivado_with_vhdl(tcl, vhdl)
