import pickle
import random
from textwrap import dedent

from jinja2 import Template

from .vivado import run_vivado_with_vhdl, vhdl_to_bitstream

BELS = ['A5FF', 'AFF', 'B5FF', 'BFF', 'C5FF', 'CFF', 'D5FF', 'DFF']
FLIPFLOPS_PER_SLICE = len(BELS)
MAX_SLICES_AT_A_TIME = 2048


def flipflops_ll(out_prefix, part, slices):
    tcl_template = """
    synth_design -top top -part {{part}} -no_srlextract
    {% for slice in slices %}
        {% set slice_index = loop.index0 %}
        {% for bel in BELS %}
            set cell [get_cells foo_reg[{{slice_index * loop.length + loop.index0}}]]
            set_property LOC {{slice}} $cell
            set_property BEL {{bel}} $cell
        {% endfor %}
    {% endfor %}
    place_design
    route_design
    set_property SEVERITY {Warning} [get_drc_checks NSTD-1]
    set_property SEVERITY {Warning} [get_drc_checks UCIO-1]
    write_bitstream -logic_location_file -force {{out_prefix}}
    """  # noqa
    tcl_template = Template(dedent(tcl_template))

    vhdl_template = """
    library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

    entity top is
        port(clk: in std_logic);
    end top;
    
    architecture Behavioral of top is
        attribute DONT_TOUCH : string;
        signal foo: std_logic_vector({{flipflop_count - 1}} downto 0) := (others => '1');
        attribute DONT_TOUCH of foo: signal is "TRUE";
    begin
        process(clk)
        begin
            if rising_edge(clk) then
                foo(foo'high downto 1) <= foo(foo'high -1 downto 0);
                foo(0) <= foo(foo'high);
            end if;
        end process;
    end Behavioral;
    """ # noqa
    vhdl_template = Template(dedent(vhdl_template))

    if len(slices) > MAX_SLICES_AT_A_TIME:
        for (i,
             start) in enumerate(range(0, len(slices), MAX_SLICES_AT_A_TIME)):
            flipflops_ll(f'{out_prefix}{i}', part,
                         slices[start:start + MAX_SLICES_AT_A_TIME])

    else:
        flipflop_count = len(slices) * FLIPFLOPS_PER_SLICE

        tcl = tcl_template.render(
            part=part, out_prefix=out_prefix, slices=slices, BELS=BELS)
        vhdl = vhdl_template.render(flipflop_count=flipflop_count)
        run_vivado_with_vhdl(tcl, vhdl)


def multiples_flipflops_sync_sr(sync_srs, out_prefix, part, slices):
    vhdl_template = """
    library UNISIM;
    use UNISIM.VComponents.all;

    entity top is
    end top;

    architecture Behavioral of top is
        attribute DONT_TOUCH : string;
        attribute LOC : string;
        attribute BEL : string;
        {% for slice in slices -%}
        {% for bel in BELS %}
        attribute DONT_TOUCH of {{slice}}_{{bel}}: label is "TRUE";
        attribute LOC of {{slice}}_{{bel}}: label is "{{slice}}";
        attribute BEL of {{slice}}_{{bel}}: label is "{{bel}}";
        {% endfor -%}
        {% endfor -%}
    begin
        {% for slice in slices -%}
            {% if sync_srs[slice] %}
                {% for bel in BELS %}
                    {% if sync_srs[(slice, bel)] %}
                        {{slice}}_{{bel}} : FDSE
                        generic map (
                            INIT => '0')
                        port map (
                            C => '0',
                            CE => '0',
                            S => '0',
                            D => '0'
                        );
                    {% else %}
                        {{slice}}_{{bel}} : FDRE
                        generic map (
                            INIT => '0')
                        port map (
                            C => '0',
                            CE => '0',
                            R => '0',
                            D => '0'
                        );
                    {% endif %}
                {% endfor %}
            {% else %}
                {% for bel in BELS %}
                    {% if sync_srs[(slice, bel)] %}
                        {{slice}}_{{bel}} : FDPE
                        generic map (
                            INIT => '0')
                        port map (
                            C => '0',
                            CE => '0',
                            PRE => '0',
                            D => '0'
                        );
                    {% else %}
                        {{slice}}_{{bel}} : FDCE
                        generic map (
                            INIT => '0')
                        port map (
                            C => '0',
                            CE => '0',
                            CLR => '0',
                            D => '0'
                        );
                    {% endif %}
                {% endfor %}
            {% endif %}
        {% endfor %}
    end Behavioral;
    """  # noqa
    vhdl_template = Template(dedent(vhdl_template))

    vhdl = vhdl_template.render(slices=slices, BELS=BELS, sync_srs=sync_srs)
    vhdl_to_bitstream(vhdl, out_prefix, part)


def random_sync_srs(slices):
    for slice in slices:
        yield ((slice), random.randrange(2))
        for bel in BELS:
            yield ((slice, bel), random.randrange(2))


def random_flipflops_sync_sr(n, out_prefix, part, slices):
    sync_srss = [dict(random_sync_srs(slices)) for _ in range(n)]

    with open(f"{out_prefix}values", 'wb') as f:
        values = []
        for slice in slices:
            name = f'{slice}.SYNC_ATTR'
            bits = ['SYNC' if sync_srs[slice] else 'ASYNC' for sync_srs in sync_srss]
            values.append((name, bits))
            for bel in BELS:
                name = f'{slice}/{bel}.FFSR'
                bits = ['SRHIGH' if sync_srs[(slice, bel)] else 'SRLOW' for sync_srs in sync_srss]
                values.append((name, bits))
        pickle.dump(values, f)

    for (i, sync_srs) in enumerate(sync_srss):
        multiples_flipflops_sync_sr(sync_srs, f'{out_prefix}{i}', part, slices)


def random_flipflops_sync_sr_region(n, out_prefix, part, start_x, end_x,
                                    start_y, end_y):
    slices = [
        f"SLICE_X{x}Y{y}"
        for x in range(start_x, end_x + 1) for y in range(start_y, end_y + 1)
    ]
    random_flipflops_sync_sr(n, out_prefix, part, slices)
