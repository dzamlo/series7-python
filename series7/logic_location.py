import attr


@attr.s
class BitLine:
    offset = attr.ib()
    frame_address = attr.ib()
    frame_offset = attr.ib()
    informations = attr.ib()

    @classmethod
    def from_line(cls, line):
        line = line.split()
        offset = int(line[1], 0)
        frame_address = int(line[2], 0)
        frame_offset = int(line[3], 0)
        informations = dict(info.split("=", 1) for info in line[4:])
        return cls(offset, frame_address, frame_offset, informations)


@attr.s
class InfoLine:
    name = attr.ib()
    value = attr.ib()

    @classmethod
    def from_line(cls, line):
        line = line.split()
        name, value = line[1].split("=", 1)
        return cls(name, value)


def parse_lines(lines, only_bitlines=False):
    for line in lines:
        if line.startswith("Bit"):
            yield BitLine.from_line(line)
        elif line.startswith("Info") and not only_bitlines:
            yield InfoLine.from_line(line)
