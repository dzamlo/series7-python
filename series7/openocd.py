import socket
import subprocess

from series7.bitstream.packet import Opcode, Type1Header
from series7.utils import reverse_bits


def launch_openocd(config_file=None):
    args = ["openocd"]
    if config_file is not None:
        args += ['-f', config_file]

    return subprocess.Popen(args)


class OpenOcdClient:
    COMMAND_SEPARATOR = b'\x1a'

    def __init__(self, host="localhost", port=6666, tap=None):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))
        self.tap = tap
        self.ircode = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        self.socket.close()

    def send_command(self, cmd):
        try:
            self.socket.sendall(cmd)
        except TypeError:
            cmd = cmd.encode('utf-8')
            self.socket.sendall(cmd)
        self.socket.sendall(self.COMMAND_SEPARATOR)
        response = b""
        while not response.endswith(self.COMMAND_SEPARATOR):
            response += self.socket.recv(4096)

        # remove the command separator at the end
        return response[:-1]

    def drscan(self, words, tap=None, end_state=None, max_len=None):
        tap = tap if tap is not None else self.tap
        if max_len is not None:
            result = []
            group = []
            for word in words:
                if len(group) == max_len:
                    result += self.drscan(group, end_state="DRPAUSE")
                    group = []
                group.append(word)
            result += self.drscan(group, end_state=end_state)
            return result
        else:
            cmd = "drscan " + tap + " " + " ".join(
                "32 0x{:08X}".format(reverse_bits(int(word), 32))
                for word in words)
            if end_state is not None:
                cmd += " -endstate " + end_state
            return self.send_command(cmd)

    def irscan(self, ircode, tap=None, end_state=None, force=False):
        tap = tap if tap is not None else self.tap
        if force or self.ircode != ircode:
            cmd = "irscan {} 0x{:X}".format(tap, ircode)
            if end_state is not None:
                cmd += " -endstate " + end_state
            self.ircode = ircode
            return self.send_command(cmd)


class XilinxOpenOcdClient(OpenOcdClient):
    NOP = 0x20000000
    SYNC = 0xAA995566
    DUMMY = 0xFFFFFFFF
    DESYNC = [0x30008001, 0x0000000D]

    def __init__(self, *args, do_init=True, tap="xc7.tap", **kwargs):
        super().__init__(*args, tap=tap, **kwargs)
        if do_init:
            self.do_init()

    def cfg_in(self, end_state=None, force=False):
        self.irscan(5, end_state=end_state, force=force)

    def cfg_out(self, end_state=None, force=False):
        self.irscan(4, end_state=end_state, force=force)

    def do_init(self):
        self.send_command("pathmove RESET RUN/IDLE")
        self.send_command("xc7_program xc7.tap")

    def sync(self, end_state=None):
        self.cfg_in()
        self.drscan([self.DUMMY, self.SYNC, self.NOP], end_state=end_state)

    def read_register(self, address, word_count=1, nb_nop=2):
        # FIXME: doesn't work correctly when interleaved with write packets
        header = Type1Header(
            header_type=1,
            opcode=Opcode.READ,
            register_address=address,
            word_count=word_count)
        self.cfg_in()
        self.drscan([self.NOP, header] + nb_nop * [self.NOP])
        self.cfg_out()
        response = self.drscan([0] * word_count)
        response = [
            reverse_bits(int(word, 16), 32) for word in response.split()
        ]
        return response

    def write_register(self, address, value):
        # TODO: -> type 2 si word_count trop grand
        try:
            value = list(value)
        except TypeError:
            value = [value]
        header = Type1Header(
            header_type=1,
            opcode=Opcode.WRITE,
            register_address=address,
            word_count=len(value))
        self.cfg_in()
        self.drscan([header] + value)

    def send_packet(self, packet, end_state=None, max_len=None):
        return self.send_packets(
            [packet], end_state=end_state, max_len=max_len)

    def send_packets(self, packets, end_state=None, max_len=None):
        self.cfg_in()
        words = []
        for packet in packets:
            words += [packet.header.bits] + packet.payload
        return self.drscan(words, end_state=end_state, max_len=max_len)
