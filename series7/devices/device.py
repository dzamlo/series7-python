from series7.bitstream.register import FrameAddress


class Device:
    def __init__(self, idcode, name, frames_addresses):
        self.idcode = idcode
        self.name = name
        self.frames_addresses = frames_addresses
        if self.frames_addresses is not None:
            self.frames_addresses = [
                FrameAddress(fa) if fa is not None else None
                for fa in frames_addresses
            ]

    @classmethod
    def from_json_data(cls, json):
        return cls(
            json.get('idcode'), json.get('name'), json.get('frames_addresses'))

    def __repr__(self):
        return '{}(idcode={!r}, name={!r}, frames_addresses={!r})'.format(
            self.__class__.__name__, self.idcode, self.name,
            self.frames_addresses)

    def for_json(self):
        if self.frames_addresses is not None:
            frames_addresses = [
                int(fa) if fa is not None else None
                for fa in self.frames_addresses
            ]
        else:
            frames_addresses = None

        return {
            'idcode': int(self.idcode),
            'name': self.name,
            'frames_addresses': frames_addresses
        }
