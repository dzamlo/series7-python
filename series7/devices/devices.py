from os.path import dirname, join
import simplejson

from .device import Device


class Devices:
    _DEFAULT_DEVICES = None

    def __init__(self, devices):
        devices = sorted(devices, key=lambda d: d.name)
        self.devices = devices

    @classmethod
    def from_json_data(cls, json):
        return cls([Device.from_json_data(device) for device in json])

    @classmethod
    def load_from_json(cls, file):
        json = simplejson.load(file)
        return cls.from_json_data(json)

    @classmethod
    def default_devices(cls, force_reload=False, use_cache=True):
        if cls._DEFAULT_DEVICES is None or force_reload or not use_cache:
            module_path = dirname(__file__)
            devices_path = join(module_path, 'devices.json')
            with open(devices_path, 'r') as f:
                devices = cls.load_from_json(f)
            if use_cache:
                cls._DEFAULT_DEVICES = devices
            return devices
        else:
            return cls._DEFAULT_DEVICES

    def __repr__(self):
        return '{}({!r})'.format(self.__class__.__name__, self.devices)

    def for_json(self):
        return sorted(self.devices, key=lambda d: d.name)

    def json_dump(self, file):
        simplejson.dump(
            self, file, for_json=True, sort_keys=True, indent=2 * ' ')

    def by_idcode(self, idcode):
        for device in self.devices:
            if device.idcode == idcode:
                return device
        return None
