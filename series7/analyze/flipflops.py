from glob import glob

from series7.bitaddress import BitAddress
from series7.bitsdb import BitsDbEntry
from series7.logic_location import parse_lines

from .misc import analyze_standard


def flipflops_ll(out_prefix):
    for fname in glob(out_prefix + "*.ll"):
        with open(fname, 'rt') as f:
            for line in parse_lines(f, only_bitlines=True):
                slice = line.informations['Block']
                latch = line.informations['Latch']
                if latch.endswith('.Q'):
                    latch = latch[:-2]
                else:
                    latch = latch[0] + "FF"

                address = BitAddress(line.frame_address,
                                     line.frame_offset // 32,
                                     line.frame_offset % 32)
                name = f'{slice}/{latch}.INIT'
                yield BitsDbEntry(address, name, True)


def random_flipflops_sync_sr(out_prefix):
    return analyze_standard(out_prefix)
