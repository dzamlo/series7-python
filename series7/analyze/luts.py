from .misc import analyze_standard


def random_luts(out_prefix):
    return analyze_standard(out_prefix)
