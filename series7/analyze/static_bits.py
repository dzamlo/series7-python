def get_static_bits(bitstreams, bitstreams_inverted):
    always_one = bitstreams[0].wordss.copy()
    for bitstream in bitstreams[1:]:
        always_one &= bitstream.wordss

    always_zero = bitstreams_inverted[0].wordss.copy()
    for bitstream in bitstreams_inverted[1:]:
        always_zero &= bitstream.wordss

    return always_one | always_zero
