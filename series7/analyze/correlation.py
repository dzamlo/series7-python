from itertools import islice

from series7.frames import Frames
from .static_bits import get_static_bits


def bit_correlate(values, bitstreams, bitstreams_inverted=None):
    frames = bitstreams[0].all_ones_like()
    frames_inverted = bitstreams[0].all_ones_like()

    if bitstreams_inverted is None:
        bitstreams_inverted = [b.inverted() for b in bitstreams]

    for (value, bitstream, bitstream_inverted) in zip(values, bitstreams,
                                                      bitstreams_inverted):
        if value:
            frames.wordss &= bitstream.wordss
            frames_inverted.wordss &= bitstream_inverted.wordss
        else:
            frames.wordss &= bitstream_inverted.wordss
            frames_inverted.wordss &= bitstream.wordss

    ones = list(islice(frames.get_ones(), 2))
    ones_inverted = list(islice(frames_inverted.get_ones(), 2))
    if len(ones) == 1 and len(ones_inverted) != 1:
        return ones[0], False
    elif len(ones) != 1 and len(ones_inverted) == 1:
        return ones_inverted[0], True


def choices_correlate(values,
                      bitstreams,
                      bitstreams_inverted=None,
                      always_static=None,
                      ignore_values={}):
    if bitstreams_inverted is None:
        bitstreams_inverted = [b.inverted() for b in bitstreams]

    if always_static is None:
        always_static = get_static_bits(bitstreams, bitstreams_inverted)

    values_set = set(v for v in values if v not in ignore_values)

    candidates = ~always_static
    for value in values_set:
        static = get_static_bits([
            b for v, b in zip(values, bitstreams) if v == value
        ], [b for v, b in zip(values, bitstreams_inverted) if v == value])
        candidates &= static

    addresses = list(Frames(bitstreams[0].addresses, candidates).get_ones())

    foo = {}
    for value in values_set:
        bitstream = bitstreams[values.index(value)]
        bit_values = [bitstream.get_bit(address) for address in addresses]
        foo[value] = bit_values

    return addresses, foo
