import pickle

import numpy as np

from series7.analyze.correlation import choices_correlate
from series7.bitsdb import BitsDbEntry
from series7.bitstream.misc import bitfile_to_frames
from series7.frames import Frames
from .static_bits import get_static_bits


def remove_static_frames(bitstreams, bitstreams_inverted):
    static_bits = get_static_bits(bitstreams, bitstreams_inverted)

    non_static_frames = np.bitwise_or.reduce(~static_bits, 1) != 0

    non_static_frames_idxs = np.nonzero(non_static_frames)[0]

    addresses = [bitstreams[0].addresses[i] for i in non_static_frames_idxs]

    for bitstream in bitstreams:
        bitstream.addresses = addresses
        bitstream.wordss = bitstream.wordss[non_static_frames_idxs]

    for bitstream in bitstreams_inverted:
        bitstream.addresses = addresses
        bitstream.wordss = bitstream.wordss[non_static_frames_idxs]


def get_values_bitstreams(*out_prefixes):
    all_values = None
    all_bitstreams = []
    for out_prefix in out_prefixes:
        with open(f'{out_prefix}values', 'rb') as f:
            values = pickle.load(f)

        bitstreams = []
        try:
            for i in range(len(values[0][1])):
                with open(f'{out_prefix}{i}.bit', 'rb') as f:
                    frames = Frames.from_frames_iter(
                        bitfile_to_frames(f.read()))
                    bitstreams.append(frames)
        except FileNotFoundError:
            # If one the bitfile cannot be found, we assume that the generation
            # hasn't finished or have been cancelled. We just continue with the
            # ones already there.
            n_bitstreams = len(bitstreams)
            for (n, v) in values:
                del v[n_bitstreams:]

        values = dict(values)

        if all_values is None:
            all_values = values
        else:
            if all_values.keys() != values.keys():
                raise ValueError(
                    "The values from all the out_prefixes must have the sames "
                    "keys")
            for (n, v) in values:
                all_values[n] += v

        all_bitstreams += bitstreams

    return (all_values, all_bitstreams)


def analyze_standard(out_prefix):
    """
    Analyze the output of a standard generate functions to identify the
    configuration bits.

    :param out_prefix: the same out_prefix that the one used in the generate
    function
    :return: A generator of the BitsDbEntry for each configuration bits found.
    """

    values, bitstreams = get_values_bitstreams(out_prefix)
    bitstreams_inverted = [bitstream.inverted() for bitstream in bitstreams]

    remove_static_frames(bitstreams, bitstreams_inverted)
    always_static = get_static_bits(bitstreams, bitstreams_inverted)

    for (name, bit_values) in values.items():
        addresses, values = choices_correlate(bit_values, bitstreams, bitstreams_inverted, always_static)
        yield BitsDbEntry(name, addresses, values)
