import attr
import numpy as np

from series7.bitaddress import BitAddress


@attr.s(slots=True)
class Frames:
    addresses = attr.ib()
    wordss = attr.ib(convert=lambda x: np.array(x, dtype='uint32'))

    @staticmethod
    def from_frames_iter(iterable):
        addresses = []
        wordss = []
        for frame in iterable:
            addresses.append(frame.address)
            wordss.append(frame.words)
        return Frames(addresses, wordss)

    def copy_wordss(self):
        return Frames(self.addresses, self.wordss.copy())

    def inverted(self):
        """return a frame with all the bit inverted"""
        return Frames(self.addresses, ~self.wordss)

    def all_ones_like(self):
        """return a Frames object with the same addresses and shape but with
        all the bits set to one"""
        return Frames(self.addresses, ~np.zeros_like(self.wordss))

    def get_ones(self):
        non_zeros = self.wordss.nonzero()
        for (y, x) in zip(*non_zeros):
            address = self.addresses[y]
            word = self.wordss[y][x]
            i = 0
            while word != 0:
                if word & 1 != 0:
                    yield BitAddress(address, x, i)
                word >>= 1
                i += 1

    def get_bit(self, address):
        for (frame_address, frame) in zip(self.addresses, self.wordss):
            if frame_address == address.frame_address:
                return int((frame[address.word] >> address.bit) & 1)
