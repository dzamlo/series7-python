# -*- coding: utf-8 -*-
import click
import simplejson

from series7.bitstream.misc import bitfile_to_frames, bitfile_to_packets, \
    bitfile_to_register_writes

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('bitstream', type=click.File('rb'), default='-')
@click.option(
    '--packets',
    'mode',
    flag_value='packets',
    default=True,
    help="Extract the packets.")
@click.option(
    '--register-writes',
    'mode',
    flag_value='register-writes',
    help="Extract writes to the registers.")
@click.option(
    '--frames',
    'mode',
    flag_value='frames',
    help="Extract the frames and their addresses.")
@click.option("--json", is_flag=True, help="Output the result as json.")
def main(bitstream, mode, json):
    """
    Parse BITSTREAM and display the packets, register writes, or frames in the
    bitstream.

    If BITSTREAM is ommited or '-', stdin is used.
    """

    buffer = bitstream.read()
    if mode == 'packets':
        elements = bitfile_to_packets(buffer)
    elif mode == 'register-writes':
        elements = bitfile_to_register_writes(buffer)
    elif mode == "frames":
        elements = bitfile_to_frames(buffer)

    if json:
        json = simplejson.dumps(list(elements), for_json=True)
        click.echo(json)
    else:
        for e in elements:
            click.echo(e)


if __name__ == '__main__':
    main()
