# -*- coding: utf-8 -*-

import sys

import click

from series7.bitstream.misc import bitfile_to_packets, \
    debug_packets_to_addresses
from series7.bitstream.register import RegisterAddress

from series7.devices import Devices

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('debug_bitstream', type=click.File('rb'))
@click.argument('devices.json', type=click.Path())
@click.argument('output.json', type=click.File('w'), required=False)
@click.option(
    '--idcode',
    type=int,
    help="Use this idcode instead of finding it in the bitstream.")
def main(debug_bitstream, idcode, **kwargs):
    """
    Get the frames addresses from a debug bitsream and add them to a
    devices.json files.

    The bitstream must be compiled with 'set_property
    BITSTREAM.GENERAL.DEBUGBITSTREAM YES [current_design]'

    If OUTPUT.JSON is omitted, the new json is written in DEVICES.JSON.
    """

    devices_json_path = kwargs['devices.json']
    output_json = kwargs['output.json']
    with click.open_file(devices_json_path, 'r') as f:
        devices = Devices.load_from_json(f)

    buffer = debug_bitstream.read()
    packets = bitfile_to_packets(buffer)

    first_lout_read = False
    if idcode is None:
        first_lout_read = True
        for packet in packets:
            if packet.register_address == RegisterAddress.IDCODE:
                idcode = packet.payload[0]
                break
        else:
            click.echo("No idcode found in the bitstream.", err=True)
            sys.exit(1)

    device = devices.by_idcode(idcode)
    if device is None:
        click.echo("No device with this idcode.", err=True)
        sys.exit(1)

    device.frames_addresses = list(
        debug_packets_to_addresses(packets, first_lout_read=first_lout_read))
    if output_json is None:
        with open(devices_json_path, 'w') as f:
            devices.json_dump(f)
    else:
        devices.json_dump(output_json)


if __name__ == '__main__':
    main()
