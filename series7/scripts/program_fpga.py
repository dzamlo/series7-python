# -*- coding: utf-8 -*-
import click

from series7 import openocd
from series7.bitstream.misc import bitfile_to_packets

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('bitstream', type=click.File('rb'), default='-')
@click.option(
    '--verbose',
    '-v',
    is_flag=True,
    help='print all the packets before sending them, can be much slower')
@click.option(
    '--host', '-h', default='localhost', help='the host of the OpenOCD server')
@click.option(
    '--port',
    '-p',
    default=6666,
    help='the port of the OpenOCD TCL RPC server')
@click.option(
    '--max-len', default=2**14, help='the maximum numbers of words per drscan')
@click.option(
    '--tap',
    '-t',
    default='xc7.tap',
    help="the name of the tap of the FPGA to program")
def main(bitstream, verbose, host, port, max_len, tap):
    """
    Parse BITSTREAM and use the packets to program a 7 series FPGA.
    Connect to an already working OpenOCD server

    For a normal use, you should prefer the "pld load" OpenOCD command.
    """
    buffer = bitstream.read()
    packets = bitfile_to_packets(buffer)
    with openocd.XilinxOpenOcdClient(host=host, port=port, tap=tap) as ocd:
        ocd.sync()
        if not verbose:
            ocd.send_packets(packets, max_len=max_len)
        else:
            for packet in packets:
                click.echo(packet)
                ocd.send_packet(packet, max_len=max_len)


if __name__ == '__main__':
    main()
