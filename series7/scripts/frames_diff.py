# -*- coding: utf-8 -*-
import click

from series7.bitstream.misc import bitfile_to_frames

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
MAX_WORD_IDX = 100
INDENT = 4 * ' '
foo = len(str(MAX_WORD_IDX))


def print_diff(frame_a, frame_b):
    click.echo(f"{frame_a.address}:")
    for (i, (worda, wordb)) in enumerate(zip(frame_a.words, frame_b.words)):
        if worda != wordb:
            click.echo(INDENT + f"{i}:")
            click.echo(2 * INDENT + f"{worda:032b}")
            click.echo(2 * INDENT + f"{wordb:032b}")


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('bitstream_a', type=click.File('rb'), default='-')
@click.argument('bitstream_b', type=click.File('rb'))
def main(bitstream_a, bitstream_b):
    """
    Show the differences between the frames of BITSTREAM_A and BITSTREAM_B
    """
    frames_a = sorted(bitfile_to_frames(bitstream_a.read()))
    frames_a = iter(frames_a)
    frames_b = sorted(bitfile_to_frames(bitstream_b.read()))
    frames_a = iter(frames_a)
    frames_b = iter(frames_b)

    try:
        frame_a = next(frames_a)
        frame_b = next(frames_b)
        while True:
            if frame_a.address == frame_b.address:
                if frame_a != frame_b:
                    print_diff(frame_a, frame_b)
                frame_a = next(frames_a)
                frame_b = next(frames_b)
            elif frame_a.address < frame_b.address:
                click.echo(f'{frame_a.address} only in BITSTREAM_A')
                frame_a = next(frames_a)
            else:
                click.echo(f'{frame_b.address} only in BITSTREAM_B')
                frame_b = next(frames_b)
    except StopIteration:
        for frame_a in frames_a:
            click.echo(f'{frame_a.address} only in BITSTREAM_A')
        for frame_b in frames_b:
            click.echo(f'{frame_b.address} only in BITSTREAM_B')


if __name__ == '__main__':
    main()
