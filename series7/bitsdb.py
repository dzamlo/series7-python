import attr
import simplejson

from series7.bitaddress import BitAddress

WORD_PER_FRAME = 101
BIT_PER_WORD = 32


@attr.s(slots=True, frozen=True)
class BitsDbEntry:
    name = attr.ib()
    addresses = attr.ib()
    values = attr.ib()

    def for_json(self):
        return self.name, [(a.frame_address.bits, a.word, a.bit)
                           for a in self.addresses], self.values

    @classmethod
    def from_json_object(cls, json_object):
        name = json_object[0]
        addresses = [
            BitAddress(frame_address, word, bit)
            for (frame_address, word, bit) in json_object[1]
        ]
        values = json_object[2]
        return cls(name, addresses, values)


class BitsDb:
    def __init__(self, entries=None):
        if entries is None:
            entries = []
        self._addr_to_name = dict()
        self._name_to_entry = dict()

        for entry in entries:
            self.insert(entry)

    @classmethod
    def load(cls, file):
        json = simplejson.load(file)
        entries = (BitsDbEntry.from_json_object(json_object)
                   for json_object in json)
        return cls(entries)

    def dump(self, file):
        simplejson.dump(self, file, for_json=True, iterable_as_array=True)

    @property
    def entries(self):
        return self._name_to_entry.values()

    def insert(self, entry):
        if entry.name in self.names or any(a in self.addresses
                                           for a in entry.addresses):
            raise ValueError("addresses and names must be unique")
        self._name_to_entry[entry.name] = entry
        for address in entry.addresses:
            self._addr_to_name[address] = entry.name

    @property
    def addresses(self):
        return self._addr_to_name.keys()

    @property
    def frames_addresses(self):
        return set(address.frame_address for address in self.addresses)

    @property
    def names(self):
        return self._name_to_entry.keys()

    def by_address(self, address):
        return self._name_to_entry[self._addr_to_name[address]]

    def by_name(self, name):
        return self._name_to_entry[name]

    def by_frame_address(self, frame_address):
        for word in range(WORD_PER_FRAME):
            for bit in range(BIT_PER_WORD):
                address = BitAddress(frame_address, word, bit)
                if address in self.addresses:
                    yield self.by_address(address)

    def for_json(self):
        return sorted(self.entries)
