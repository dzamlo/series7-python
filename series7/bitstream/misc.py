from functools import total_ordering
from struct import Struct

import attr
import numpy as np

from series7.bitstream.packet import Opcode, Packet, merge_packets
from series7.bitstream.register import FrameAddress, Register, RegisterAddress
from series7.devices import Devices
from series7.utils import grouper, pairwise

SYNC_WORD = b'\xAA\x99\x55\x66'
WORD_STRUCT = Struct(">I")
FRAME_SIZE = 101
WORD_BITSIZE = 32


@total_ordering
@attr.s(slots=True, cmp=False)
class Frame:
    address = attr.ib(convert=FrameAddress)
    words = attr.ib(convert=lambda x: np.array(x, dtype='uint32'))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.address == other.address and np.array_equal(
                self.words, other.words)
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            if self.address < other.address:
                return True
            elif self.address > other.address:
                return False
            else:
                for (word_self, word_other) in zip(self.words, other.words):
                    if word_self < word_other:
                        return False
                    elif word_self > word_other:
                        return True
                return False
        else:
            return NotImplemented

    def for_json(self):
        return {'address': self.address, 'words': self.words.tolist()}


def find_sync_word(buffer):
    return buffer.find(SYNC_WORD)


def buffer_to_words(buffer):
    return (word[0] for word in WORD_STRUCT.iter_unpack(buffer))


def bitfile_to_words(buffer):
    sync_word_pos = find_sync_word(buffer)
    if sync_word_pos == -1:
        raise ValueError("Sync word not found")
    return buffer_to_words(buffer[sync_word_pos + WORD_STRUCT.size:])


def bitfile_to_packets(buffer):
    words = bitfile_to_words(buffer)
    try:
        while True:
            yield Packet.from_iter(words)
    except StopIteration:
        pass


def bitfile_to_register_writes(buffer):
    packets = bitfile_to_packets(buffer)
    merged_packets = merge_packets(packets)
    for packet in merged_packets:
        if packet.header.opcode == Opcode.WRITE:
            yield Register(packet.header.register_address, packet.payload)


def debug_register_writes_to_frames(register_writes):
    fdri = None
    for register in register_writes:
        if register.address == RegisterAddress.FDRI:
            fdri = register.value
        elif register.address == RegisterAddress.LOUT and fdri is not None:
            yield Frame(register.value[0], fdri)


def debug_bitfile_to_frames(buffer):
    return debug_register_writes_to_frames(bitfile_to_register_writes(buffer))


def debug_packets_to_addresses(packets, first_lout_read=False):
    type_0 = 0

    for packet in packets:
        if packet.header_type == 0:
            type_0 += 1
            if type_0 == FRAME_SIZE:
                yield None
                type_0 = 0
        elif packet.register_address == RegisterAddress.LOUT:
            if first_lout_read:
                yield FrameAddress(packet.payload[0])
            first_lout_read = True
            type_0 = 0


def debug_bitfile_to_addresses(buffer, first_lout_read=False):
    return debug_packets_to_addresses(
        bitfile_to_packets(buffer), first_lout_read=first_lout_read)


def register_writes_to_frames(register_writes, addresses=None, devices=None):
    # Most of the complexity comes from the fact that we want to support
    # normal, debug and perframecrc bitstresm.
    far_idx = None
    if addresses is None and devices is None:
        devices = Devices.default_devices()
    for register in register_writes:
        if register.address == RegisterAddress.LOUT:
            yield from debug_register_writes_to_frames(register_writes)
            return
        elif addresses is None and register.address == RegisterAddress.IDCODE:
            idcode = register.value
            device = devices.by_idcode(idcode)
            if device is None:
                raise ValueError(
                    f"No device with the idcode {hex(int(idcode))}")
            addresses = device.frames_addresses
            if addresses is None:
                raise ValueError(f"No frames_addresses for {device}")
        elif register.address == RegisterAddress.FDRI:
            for (address, frame) in zip(addresses[far_idx:],
                                        grouper(register.value, FRAME_SIZE)):
                if address is not None:
                    yield Frame(address, frame)
        elif register.address == RegisterAddress.FAR:
            far = register.value
            try:
                far_idx = addresses.index(far, far_idx)
            except (ValueError, TypeError):
                try:
                    far_idx = addresses.index(far)
                except ValueError:
                    far_idx = None
        elif register.address == RegisterAddress.CRC:
            if far_idx is not None:
                far_idx += 1


def bitfile_to_frames(buffer, addresses=None, devices=None):
    return register_writes_to_frames(
        bitfile_to_register_writes(buffer), addresses, devices)


def frames_apply(func, *args):
    """apply func on each words of each frames"""
    for frames in zip(*args):
        assert all(a.address == b.address for (a, b) in pairwise(frames)),\
            "All the frames must have the same addresses."

        words = func(*(frame.words for frame in frames))
        yield Frame(frames[0].address, words)


def frames_apply_each_words(func, *args):
    """apply func on each words of each frames"""
    for frames in zip(*args):
        assert all(a.address == b.address for (a, b) in pairwise(frames)),\
            "All the frames must have the same addresses."

        words = [
            func(*input_words)
            for input_words in zip(*(frame.words for frame in frames))
        ]
        yield Frame(frames[0].address, words)


def frames_filter(func, frames):
    """like filter, but applied to the words instead of the whole frames
    tuples"""
    for frame in frames:
        if func(frame.words):
            yield frame


def frames_non_zero(frames):
    """returns only the non zero frames"""
    return frames_filter(any, frames)
