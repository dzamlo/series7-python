from itertools import islice

import attr

from series7.bitfield import IntBitfield
from series7.bitstream.register import RegisterAddress
from series7.utils import OpenIntEnum, pairwise_longest


class Opcode(OpenIntEnum):
    NOP = 0b00
    READ = 0b01
    WRITE = 0b10
    RESERVED = 0b11


class UnknownHeader(IntBitfield):
    _bitfields = {'header_type': (31, 29)}

    @property
    def word_count(self):
        return 0


class Type1Header(IntBitfield):
    _bitfields = {
        'header_type': (31, 29),
        'opcode': (28, 27, Opcode, int),
        'register_address': (26, 13, RegisterAddress, int),
        'word_count': (10, 0)
    }


class Type2Header(IntBitfield):
    _bitfields = {
        'header_type': (31, 29),
        'opcode': (28, 27, Opcode, int),
        'word_count': (26, 0)
    }


def u32_to_header(value):
    header = UnknownHeader(value)
    if header.header_type == 1:
        return Type1Header(value)
    elif header.header_type == 2:
        return Type2Header(value)
    else:
        return header


@attr.s
class Packet():
    header = attr.ib()
    payload = attr.ib()

    @classmethod
    def from_iter(cls, words_iterator):
        header = u32_to_header(next(words_iterator))
        payload = list(islice(words_iterator, header.word_count))
        return cls(header, payload)

    @property
    def header_type(self):
        return self.header.header_type

    @property
    def register_address(self):
        try:
            return self.header.register_address
        except AttributeError:
            return None

    def for_json(self):
        return attr.asdict(self)


def merge_packets(packets_iterable):
    """merge type 1 and type 2 packet together

    This function assume that the packets come from a well formed bitstream
    and will yield incorrect result if it not the case
    """
    for (packet1, packet2) in pairwise_longest(packets_iterable):
        try:
            if packet2.header_type == 2:
                packet1.payload += packet2.payload
        except AttributeError:
            pass

        if packet1.header_type == 1:
            yield packet1
