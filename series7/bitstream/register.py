import attr

from series7.bitfield import IntBitfield
from series7.utils import OpenIntEnum


class RegisterAddress(OpenIntEnum):
    CRC = 0b00000
    FAR = 0b00001
    FDRI = 0b00010
    FDRO = 0b00011
    CMD = 0b00100
    CTL0 = 0b00101
    MASK = 0b00110
    STAT = 0b00111
    LOUT = 0b01000
    COR0 = 0b01001
    MFWR = 0b01010
    CBC = 0b01011
    IDCODE = 0b01100
    AXSS = 0b01101
    COR1 = 0b01110
    WBSTAR = 0b10000
    TIMER = 0b10001
    BOOTSTS = 0b10110
    CTL1 = 0b11000
    BSPI = 0b11111
    BSPI_READ = 0b10010
    FALL_EDGE = 0b10011


class BlockType(OpenIntEnum):
    CLB_IO_CLK = 0b000
    BLOCK_RAM = 0b001
    CFG_CLB = 0b010


class FrameAddress(IntBitfield):
    _bitfields = {
        'block_type': (25, 23, BlockType, int),
        'bottom': 22,
        'row_address': (21, 17),
        'column_address': (16, 7),
        'minor_address': (6, 0)
    }


class Command(OpenIntEnum):
    NULL = 0b00000
    WCFG = 0b00001
    MFW = 0b00010
    DGHIGH_LFRM = 0b00011
    RCFG = 0b00100
    START = 0b00101
    RCAP = 0b00110
    RCRC = 0b00111
    AGHIGH = 0b01000
    SWITCH = 0b01001
    GRESTORE = 0b01010
    SHUTDOWN = 0b01011
    GCAPTURE = 0b01100
    DESYNC = 0b01101
    RESERVED = 0b01110
    IPROG = 0b01111
    CRCC = 0b10000
    LTIMER = 0b10001


class Control0(IntBitfield):
    _bitfields = {
        'efuse_key': (31),
        'icap_select': (30),
        'over_temp_power_down': (12),
        'config_fallback': (10),
        'glutmask_b': (8),
        'farsrc': (7),
        'dec': (6),
        'sbits': (5, 4),
        'persist': (3),
        'gts_usr_b': (0),
    }


class Status(IntBitfield):
    _bitfields = {
        'bus_width': (26, 25),
        'startup_state': (20, 18),
        'dec_error': (16),
        'id_error': (15),
        'done': (14),
        'release_done': (13),
        'init_b': (12),
        'init_complete': (11),
        'mode': (10, 8),
        'ghigh_b': (7),
        'gwe': (6),
        'gts_cfg_b': (5),
        'eos': (4),
        'dci_match': (3),
        'mmcm_lock': (2),
        'part_secured': (1),
        'crc_error': (0),
    }


class ConfigurationOptions0(IntBitfield):
    _bitfields = {
        'pwrdwn_stat': (27),
        'done_pipe': (25),
        'drive_done': (24),
        'single': (23),
        'oscfsel': (22, 17),
        'ssclksrc': (16, 15),
        'done_cycle': (14, 12),
        'match_cycle': (11, 9),
        'lock_cycle': (8, 6),
        'gts_cycle': (5, 3),
        'gwe_cycle': (2, 0),
    }


class ConfigurationOptions1(IntBitfield):
    _bitfields = {
        'persist_deassert_at_desync': (17),
        'rbcrc_action': (16, 15),
        'rbcrc_no_pin': (9),
        'rbcrc_en': (8),
        'bpi_1st_read_cycle': (3, 2),
        'bpi_page_size': (1, 0),
    }


class WarmBootStartAdress(IntBitfield):
    _bitfields = {
        'rs': (31, 30),
        'rs_ts_b': (29),
        'start_addr': (28, 0),
    }


class WatchdogTimer(IntBitfield):
    _bitfields = {
        'timer_usr_mon': (31),
        'timer_cfg_mon': (30),
        'timer_value': (29, 0),
    }


class BootHistoryStatus(IntBitfield):
    _bitfields = {
        'hmac_error_1': (15),
        'wrap_error_1': (14),
        'crc_error_1': (13),
        'id_error_1': (12),
        'wto_error_1': (11),
        'iprog_1': (10),
        'fallback_1': (9),
        'valid_1': (8),
        'hmac_error_0': (7),
        'wrap_error_0': (6),
        'crc_error_0': (5),
        'id_error_0': (4),
        'wto_error_0': (3),
        'iprog_0': (2),
        'fallback_0': (1),
        'valid_0': (0),
    }


class IdCode(IntBitfield):
    _bitfields = {
        'version': (31, 28),
        'family': (27, 21),
        'array': (20, 12),
        'sub_family': (20, 17),
        'device': (16, 12),
        'company': (11, 1),
    }


REGISTER_ADDRESS_TO_TYPE = {
    RegisterAddress.FAR: FrameAddress,
    RegisterAddress.CMD: Command,
    RegisterAddress.CTL0: Control0,
    RegisterAddress.STAT: Status,
    RegisterAddress.COR0: ConfigurationOptions0,
    RegisterAddress.IDCODE: IdCode,
    RegisterAddress.COR1: ConfigurationOptions1,
    RegisterAddress.WBSTAR: WarmBootStartAdress,
    RegisterAddress.TIMER: WatchdogTimer,
    RegisterAddress.BOOTSTS: BootHistoryStatus,
}


def convert_value(address, value):
    try:
        return REGISTER_ADDRESS_TO_TYPE[address](value[0])
    except KeyError:
        return value


@attr.s
class Register:
    address = attr.ib()
    value = attr.ib()

    def __attrs_post_init__(self):
        self.value = convert_value(self.address, self.value)

    def for_json(self):
        return attr.asdict(self)
