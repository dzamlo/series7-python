class BitfieldField(object):
    def __init__(self, msb, lsb=None, from_bits=None, to_bits=None):
        self.msb = msb
        self.from_bits = from_bits
        self.to_bits = to_bits
        if lsb is None:
            self.lsb = msb
            if self.from_bits is None:
                self.from_bits = bool
            if self.to_bits is None:
                self.to_bits = int
        else:
            self.lsb = lsb
            if self.from_bits is None:
                self.from_bits = lambda x: x
            if self.to_bits is None:
                self.to_bits = lambda x: x

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        bits = obj.get_bits(self.msb, self.lsb)
        return self.from_bits(bits)

    def __set__(self, obj, value):
        bits = self.to_bits(value)
        obj.set_bits(self.msb, self.lsb, bits)


class Bitfield:
    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        dct = cls.__dict__
        if '_bitfields' in dct:
            for (field_name, field_description) in dct['_bitfields'].items():
                try:
                    setattr(cls, field_name, BitfieldField(
                        **field_description))
                except TypeError:
                    try:
                        setattr(cls, field_name,
                                BitfieldField(*field_description))
                    except TypeError:
                        setattr(cls, field_name,
                                BitfieldField(field_description))

            if '__repr__' not in dct:

                def __repr__(self):
                    fields_values = [
                        field_name + '=' + repr(getattr(self, field_name))
                        for field_name in dct['_bitfields']
                    ]
                    fields_values.append('bits=' + repr(self.bits))
                    fields_repr = ", ".join(fields_values)
                    return "{}({})".format(self.__class__.__name__,
                                           fields_repr)

                cls.__repr__ = __repr__

            if 'for_json' not in dct:

                def for_json(self):
                    fields_values = {
                        field_name: getattr(self, field_name)
                        for field_name in dct['_bitfields']
                    }
                    fields_values['bits'] = self.bits
                    return fields_values

                cls.for_json = for_json

    def __init__(self, bits, **kwargs):
        self.bits = bits
        for (key, value) in kwargs.items():
            setattr(self, key, value)

    def get_bits(self, msb, lsb):
        raise NotImplementedError()

    def set_bits(self, msb, lsb, value):
        raise NotImplementedError()


class IntBitfield(Bitfield):
    def __init__(self, bits=0, **kwargs):
        super().__init__(int(bits), **kwargs)

    def __int__(self):
        return self.bits

    def __eq__(self, other):
        return self.bits == other

    def __hash__(self):
        return hash(self.bits)

    def __lt__(self, other):
        return self.bits < other.bits

    def get_bits(self, msb, lsb):
        length = msb - lsb + 1
        mask = (1 << length) - 1
        return self.bits >> lsb & mask

    def set_bits(self, msb, lsb, value):
        length = msb - lsb + 1
        mask = (1 << length) - 1

        self.bits &= ~(mask << lsb)
        self.bits |= (value & mask) << lsb
