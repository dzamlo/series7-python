import attr

from series7.bitstream.register import FrameAddress


@attr.s(slots=True, frozen=True)
class BitAddress:
    frame_address = attr.ib(convert=FrameAddress)
    word = attr.ib(convert=int)
    bit = attr.ib(convert=int)

    def for_json(self):
        return attr.asdict(self)
