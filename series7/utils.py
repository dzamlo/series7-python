from enum import Enum
from itertools import chain, tee, zip_longest


class OpenIntEnum(Enum):
    @classmethod
    def _missing_(cls, value):
        return value

    def __int__(self):
        return self.value

    def for_json(self):
        return self.name


# function from https://docs.python.org/3.6/library/itertools.html
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def pairwise_longest(iterable, fillvalue=None):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..., (sn-1, sn), (sn, fillvalue)"
    a, b = tee(iterable)
    next(b, None)
    return zip_longest(a, b, fillvalue=fillvalue)


def iterable_to_ranges(iterable, key=lambda x: x):
    """
    >>> list(iterable_to_ranges([0, 2, 3, 4, 5, 8, 9, 10, 12]))
    [(0, 0), (2, 5), (8, 10), (12, 12)]
    """
    iterable = iter(iterable)
    range_start = next(iterable)
    for (a, b) in pairwise_longest(chain([range_start], iterable)):
        if b is None or key(b) != key(a) + 1:
            yield (range_start, a)
            range_start = b


def reverse_bits(value, bit_length):
    result = 0
    for i in range(bit_length):
        result <<= 1
        result |= value & 1
        value >>= 1
    return result


# function from https://docs.python.org/3.6/library/itertools.html
def grouper(iterable, n, fillvalue=None):
    """
    >>> list(grouper('ABCDEFG', 3, 'x'))
    [('A', 'B', 'C'), ('D', 'E', 'F'), ('G', 'x', 'x')]
    """
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def sequence_chunks(sequence, n):
    """
    >>> list(sequence_chunks([1, 2, 3, 4, 5, 6, 7], 3))
    [[1, 2, 3], [4, 5, 6], [7]]
    >>> list(sequence_chunks((1, 2, 3, 4, 5, 6, 7), 3))
    [(1, 2, 3), (4, 5, 6), (7,)]
    >>> list(sequence_chunks('ABCDEFG', 3))
    ['ABC', 'DEF', 'G']
    """
    for i in range(0, len(sequence), n):
        yield sequence[i:i + n]
