# series7

Reverse engineering of the 7 series FPGA bitstream.

To run the scripts in `series7/scripts`, install the package with the following command:

```sh
pip install --user -e .
```

The scripts should the be available in your PATH.

The reverse engineering things is mainly composed of two parts: generator and analyzer

The generators are in the `series7.generate` module.

For example, to generate bitstreams to find the bits for the luts of `SLICE_X0_Y0`:
```python
from series7 import generate
generate.luts.random_luts_region(10, '/tmp/random-luts-', 'xc7a50ticsg324-1l', 0,0,0,0)
```

For most generator, you can analyze the generated bitstream with the `analyze_standard` function. For example:

```python
 from series7 import analyze
 entries = analyze.misc.analyze_standard('/tmp/random-luts-')
 ```

`entries` is a generator of `BitsDbEntry`. Each entry represent one values.

They can be managed by `BitsDb`, for example:

```python
from series7.bitsdb import BitsDb, BitsDbEntry
db = BitsDb.load(open('7a50-bitsdb.json')) # or just BitsDb() for an empty database
for entry in entries: 
    db.insert(entry) 
db.dump(open("7a50-bitsdb.json", "w"))
```

## Adding a new part

To use a part, it need to be included in `series7/devices/devices.json`.
To do that you need to:
* Generate a bitstream for this part `set_property
    BITSTREAM.GENERAL.DEBUGBITSTREAM YES [current_design]`
* Add the part with an empty `frames_addresses` in the json file.
* Use `add_frames_addresses_to_json` to add the addresses to `devices.json`

## OpenOCD

There is also code to use OpenOCD for low-level JTAG interaction with an FPGA. See `series7/scripts/program_fpga.py` for some example on how to use that.
